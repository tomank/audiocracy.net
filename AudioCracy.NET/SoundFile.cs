﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AudioCracy.NET
{
    public class SoundFile
    {
        public String id = null;
        public String url = " ";
        public String title { get; set; } = " ";
        public String author { get; set; } = " ";
        public String thumbnail { get; set; }  = " ";
        public int duration = 0;
        public String website = " ";
        public BitmapImage cover = null;

        public int time = 0;
        public int downvotes = 0;
        public string showtime { get; set; }

        public SoundFile(String _id, String _url, String _title, String _author, String _thumbnail, int _duration, String _website)
        {
            id = _id;
            url = _url;
            title = _title;
            author = _author;
            thumbnail = _thumbnail;
            duration = _duration;
            website = _website;
            cover = GetImage(thumbnail);
            getTimeToString();
        }
        
        public SoundFile()
        {
            cover = GetImage("https://i.ytimg.com/vi/a4JlkKNA3yw/maxresdefault.jpg");
        }

        private BitmapImage GetImage(string imageUri)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri(imageUri, UriKind.RelativeOrAbsolute);
            bitmapImage.EndInit();
            return bitmapImage;
        }


        private void getTimeToString()
        {
            showtime = TimeSpan.FromSeconds(duration).ToString();
        }

        public String getCurrentTime()
        {
            TimeSpan t_current = TimeSpan.FromSeconds(time);
            TimeSpan t_duration = TimeSpan.FromSeconds(duration);
            String result = "";
            if (t_duration.Hours>0)
            {
                if(t_current.Hours<10)
                {
                    result += 0;
                }
                result += t_current.Hours + ":";
            }


            if (t_current.Minutes < 10)
            {
                result += "0";
            }
            result += t_current.Minutes + ":";

            if (t_current.Seconds < 10)
            {
                result += "0";
            }

            result += t_current.Seconds + "/";

            if (t_duration.Hours > 0)
            {
                if(t_duration.Hours < 10)
                {
                    result += 0;
                }
                result += t_duration.Hours + ":";
            }

            if (t_duration.Minutes < 10)
            {
                result += 0;
            }

            result += t_duration.Minutes+ ":";

            if (t_duration.Seconds < 10)
            {
                result += 0;
            }

            result += t_duration.Seconds;

            return result;

        }


       
    }
}

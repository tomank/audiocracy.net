using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace AudioCracy.NET
{
    //Nudzi ci się już Mati XD
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WebCtrl ctrl = new WebCtrl("192.168.1.33");
    //    WebCtrl ctrl = new WebCtrl("127.0.0.1");
        PlayList window = null;
        private static System.Timers.Timer aTimer;
        public MainWindow()
        {
            InitializeComponent();
            this.Topmost = true;
            this.Top = 20;
            this.Left = System.Windows.SystemParameters.PrimaryScreenWidth - 490;
            ctrl.connect();
            SetTimer(setTimeBar);
            LocationChanged += new System.EventHandler(this.statusStrip1_LocationChanged);
        }




        private static void OnTimedEvent(Object source, ElapsedEventArgs e, Func<int, int> setTimeBar)
        {
            setTimeBar(1);
        }



        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                aTimer.Stop();
                aTimer.Dispose();

            }
            catch (Exception ee)
            {

            }
            if (window != null)
            {
                window.Close();
            }
            ctrl.closeConn();
            ctrl = null;
        }
        private static void SetTimer(Func<int, int> myMethodName)
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e, myMethodName);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

        }




        public int setTimeBar(int i)
        {

            this.Dispatcher.Invoke(() => {
                String status = "Close";
                try { status = ctrl.webSocket.State.ToString(); }
                catch (Exception e) { }


                if (status == "Open")
                {
                    Debug.WriteLine(ctrl.msg);
                    try
                    {
                        JObject jObject = JObject.Parse(ctrl.msg);
                        if (jObject.ContainsKey("queue"))
                        {
                            ctrl.AddTracksToQueue(jObject);
                            RefreshListWindow();
                        }
                       

                        if (jObject.ContainsKey("time"))
                        {
                            ctrl.current.time = Int32.Parse(jObject.SelectToken("time").ToString());
                            refreshCurrentTime(ctrl.current);
                        }

                        if (jObject.ContainsKey("downvotes") || jObject.ContainsKey("type"))
                        {
                            ctrl.RefreshDownvotes(jObject);
                            refreshVote(ctrl.current);
                        }


                        if (jObject.ContainsKey("current"))
                        {
                            ctrl.AddCurrentTrack(jObject);
                            refreshCurrentTrack(ctrl.current);
                        }

                    }
                    catch (Exception e_json)
                    {
                        ctrl.GetState();
                    }
                }
              
                ctrl.getRec();
            });

            return 0;
        }


        private void refreshCurrentTrack(SoundFile current)
        {
            SoundTitle.Text = current.title;
            Author.Text = current.author;
            SoundTime.Text = current.getCurrentTime();
            ProgressTime.Maximum = current.duration;
            Cover.Source = current.cover;
        }

        private void refreshCurrentTime(SoundFile current)
        {
            SoundTime.Text = current.getCurrentTime();
            ProgressTime.Value = current.time;
        }

        private void refreshVote(SoundFile current)
        {
            DownvoteBtn.Content = "Downvotes (" + current.downvotes + ")";
        }

        private int lastTrackCnt = 0;

        private void RefreshListWindow()
        {
            if(lastTrackCnt!=ctrl.soundList.Count)
            {
                lastTrackCnt = ctrl.soundList.Count;
                if(window!=null)
                {
                    window.Close();
                    window = null;
                    window = new PlayList(Application.Current.MainWindow.Left + 10, Application.Current.MainWindow.Top + Application.Current.MainWindow.Height);
                    window.AdToList(ctrl.soundList);
                    window.Show();
                }
            }
        }




        public int setTimeBar2(int i)
        {
            this.Dispatcher.Invoke(() =>
            {
                try
                {
                    String status = ctrl.webSocket.State.ToString();

                    if (status == "Open")
                    {

                        String data = ctrl.GetData();
                        
                        if (data != null)
                        {
                            JObject currencies = JObject.Parse(data);
                            if (currencies.ContainsKey("queue"))
                            {
                                var newFiles = currencies.SelectToken("queue");
                                ctrl.ClearList();
                                foreach (var file in newFiles)
                                {
                                    ctrl.AddFile(new SoundFile(
                                        file.SelectToken("id").ToString(),
                                        file.SelectToken("url").ToString(),
                                        file.SelectToken("title").ToString(),
                                        file.SelectToken("author").ToString(),
                                        file.SelectToken("thumbnail").ToString(),
                                        Int32.Parse(file.SelectToken("duration").ToString()),
                                        file.SelectToken("website").ToString()
                                     ));
                                }

                            }

                            if (window != null && ctrl.soundList.Count != lastTrackCnt)
                            {
                                lastTrackCnt = ctrl.soundList.Count;
                                window.Close();
                                window = new PlayList(Application.Current.MainWindow.Left + 10, Application.Current.MainWindow.Top + Application.Current.MainWindow.Height);
                                window.AdToList(ctrl.soundList);
                                window.Show();
                            }

                            if (currencies.ContainsKey("current"))
                            {
                                try
                                {
                                    var current = currencies.SelectToken("current");
                                    ctrl.current = new SoundFile(
                                        current.SelectToken("id").ToString(),
                                        current.SelectToken("url").ToString(),
                                        current.SelectToken("title").ToString(),
                                        current.SelectToken("author").ToString(),
                                        current.SelectToken("thumbnail").ToString(),
                                        Int32.Parse(current.SelectToken("duration").ToString()),
                                        current.SelectToken("website").ToString()
                                     );
                                }
                                catch (Exception ex)
                                {

                                }

                            }
                            if (currencies.ContainsKey("time"))
                            {
                                try
                                {
                                    ctrl.current.time = Int32.Parse(currencies.SelectToken("time").ToString());
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                            if (currencies.ContainsKey("downvotes"))
                            {
                                try
                                {
                                    ctrl.current.downvotes = Int32.Parse(currencies.SelectToken("downvotes").ToString());
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                            if (currencies.ContainsKey("type"))
                            {
                                try
                                {
                                    String type = currencies.SelectToken("type").ToString();
                                    if (type == "downvotes")
                                    {
                                        ctrl.current.downvotes = Int32.Parse(currencies.SelectToken("count").ToString());
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                            ctrl.getRec();
                        }
                    }
                  //  Title.Text = ctrl.current.title;
                 //   Cover.Source = ctrl.current.cover;
                 //   Author.Text = ctrl.current.author;
                //    ProgressTime.Maximum = ctrl.current.duration;
                    if (ProgressTime.Value == ctrl.current.time && ctrl.current.duration!=0 && ctrl.current.time!=0)
                    {
                       // ctrl.GetState();
                    }
                    else
                    {
                        ProgressTime.Value = ctrl.current.time;
                    }
                       
                    DownvoteBtn.Content = "Downvotes (" + ctrl.current.downvotes + ")";
                    SoundTime.Text = ctrl.current.getCurrentTime();
                }
                catch (Exception e)
                {
                    int b = 0;
                }
            });
       
            return 0;
        }

        //{"type":"downvotes","count":2}
        private void Down_Click(object sender, RoutedEventArgs e)
        {
            ctrl.Downvote();
        }


        private void statusStrip1_LocationChanged(object sender, EventArgs e)
        {
            if (window != null)
            {
                window.Left = Application.Current.MainWindow.Left + 10;
                window.Top = Application.Current.MainWindow.Top + Application.Current.MainWindow.Height;
            }
        }


        private void Add_Click(object sender, RoutedEventArgs e)
        {
            ctrl.AddTrack();
        }

        private void List_Click(object sender, RoutedEventArgs e)
        {
            if(window==null)
            {
                window = new PlayList(Application.Current.MainWindow.Left+10, Application.Current.MainWindow.Top + Application.Current.MainWindow.Height);
                if(ctrl.soundList.Count>0)
                {
                    window.AdToList(ctrl.soundList);
                }
                window.Show();
            }
            else
            {
                window.Close();
                window = null;
            }
            
        }

        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            if(window != null) window.Close();
            Close();
        }

        private void MiniWindow_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
            if (window != null)
            {
                window.Close();
                window = null;
            }
        }
    }
}

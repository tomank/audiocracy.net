﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.WebSockets;
using System.Threading;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows;

namespace AudioCracy.NET
{
    public class WebCtrl
    {
        public List<SoundFile> soundList = new List<SoundFile>();
        public ClientWebSocket webSocket = null;
        private String address = null;

        private static object consoleLock = new object();
        private const int sendChunkSize = 999999;
        private const int receiveChunkSize = 999999;
        private const bool verbose = true;
        private static readonly TimeSpan delay = TimeSpan.FromMilliseconds(30000);
        public byte[] buffer = new byte[receiveChunkSize];
        public String msg = null;
        public SoundFile current = new SoundFile();


        public WebCtrl(String _addr)
        {
            address = _addr;
        }

        public async Task connect()
        {
            Debug.WriteLine("Dupa debug");
            try
            {
                webSocket = new ClientWebSocket();
                await webSocket.ConnectAsync(new Uri("ws://" + address + ":9000/ws"), CancellationToken.None);
                //await Task.WhenAll(Receive(webSocket, buffer));

                //    ReceiveVoid(webSocket, buffer, msg);

                getRec();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }


        public String GetData()
        {
            String data = System.Text.Encoding.Default.GetString(buffer);
            if (msg == null || msg.Length == 0)
            {
                return null;
            }
            return msg;
        }


        static UTF8Encoding encoder = new UTF8Encoding();
        private static async Task Send(ClientWebSocket webSocket, String msg)
        {

            byte[] buffer_s = encoder.GetBytes(msg);
            await webSocket.SendAsync(new ArraySegment<byte>(buffer_s), WebSocketMessageType.Text, true, CancellationToken.None);


            while (webSocket.State == WebSocketState.Open)
            {
                //  LogStatus(false, buffer, buffer.Length);
                await Task.Delay(delay);

            }
        }

        public void Downvote()
        {
            string output = JsonConvert.SerializeObject(new BaseClass("downvote"));
            Send(webSocket, output);
        }

        public void GetState()
        {
            string output = JsonConvert.SerializeObject(new BaseClass("state"));
            Send(webSocket, output);
        }

        public async void getRec()
        {
            try
            {
                msg = ReceiveVoid(webSocket, buffer);
            }
            catch (Exception ee)
            {

            }

        }

        public void AddTrack()
        {
            String url = Clipboard.GetText();
            if (url.IndexOf("https") > -1)
            {
                string output = JsonConvert.SerializeObject(new NewTrack(url));
                Send(webSocket, output);
            }
        }



        public async void sendRec(String msg)
        {
            if (webSocket != null)
            {
                await Send(webSocket, msg);
            }

        }

        private static async Task Receive(ClientWebSocket webSocket, byte[] buffera, String msg)
        {
            buffera = new byte[receiveChunkSize];
            for (int i = 0; i < receiveChunkSize; i++)
            {
                buffera[i] = 0;
            }
            while (webSocket.State == WebSocketState.Open)
            {
                var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffera), CancellationToken.None);

                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                }
                else
                {
                    if (result.EndOfMessage)
                    {
                        msg = System.Text.Encoding.Default.GetString(buffera);

                        break;
                    }
                }
            }
        }

        private static String ReceiveVoid(ClientWebSocket webSocket, byte[] buffera)
        {
            buffera = new byte[receiveChunkSize];
            for (int i = 0; i < receiveChunkSize; i++)
            {
                buffera[i] = 0;
            }
            while (webSocket.State == WebSocketState.Open)
            {
                Task<WebSocketReceiveResult> result = webSocket.ReceiveAsync(new ArraySegment<byte>(buffera), CancellationToken.None);
                result.Wait(1000);
                return System.Text.Encoding.Default.GetString(buffera);

            }
            return null;
        }

        public void AddTracksToQueue(JObject jObject)
        {
            var newTracks = jObject.SelectToken("queue");
            ClearList();
            foreach(var track in newTracks)
            {
                AddFile(new SoundFile(
                            track.SelectToken("id").ToString(),
                            track.SelectToken("url").ToString(),
                            track.SelectToken("title").ToString(),
                            track.SelectToken("author").ToString(),
                            track.SelectToken("thumbnail").ToString(),
                            Int32.Parse(track.SelectToken("duration").ToString()),
                            track.SelectToken("website").ToString()
                       )); 
            }
        }

        public void AddCurrentTrack(JObject jObject)
        {
            var currentJson = jObject.SelectToken("current");
              current = new SoundFile(
                    currentJson.SelectToken("id").ToString(),
                    currentJson.SelectToken("url").ToString(),
                    currentJson.SelectToken("title").ToString(),
                    currentJson.SelectToken("author").ToString(),
                    currentJson.SelectToken("thumbnail").ToString(),
                    Int32.Parse(currentJson.SelectToken("duration").ToString()),
                    currentJson.SelectToken("website").ToString()
            );
        }

        public void RefreshTime(JObject jObject)
        {

        }

        public void RefreshDownvotes(JObject jObject)
        {
            if (jObject.ContainsKey("downvotes"))
            {
                current.downvotes = Int32.Parse(jObject.SelectToken("downvotes").ToString());
            }
            if (jObject.ContainsKey("type"))
            {
                String type = jObject.SelectToken("type").ToString();
                if (type == "downvotes")
                {
                    current.downvotes = Int32.Parse(jObject.SelectToken("count").ToString());
                }
            }
        }


        public void AddFile(SoundFile newFile)
        {
            soundList.Add(newFile);
        }


        public void ClearList()
        {
            soundList.Clear();
        }

        public void closeConn()
        {
            String status = webSocket.State.ToString();

            if (status == "Open")
            {
                webSocket.Abort();
            }
        }
    }

   
    public class BaseClass
    {
        public string type;
        public BaseClass(String _type)
        {
            type = _type;
        }
    }

    public class NewTrack
    {
        public string type;
        public string url;
        public NewTrack(String _url)
        {
            type = "add";
            url = _url;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AudioCracy.NET
{
    /// <summary>
    /// Logika interakcji dla klasy PlayList.xaml
    /// </summary>
    public partial class PlayList : Window
    {

        public PlayList(double left, double top)
        {
            InitializeComponent();
            this.Left = left;
            this.Top = top;
            this.Topmost = true;
        }

        public void AdToList(List<SoundFile> tracks)
        {
            lvDataBinding.ItemsSource = tracks;
        }


    }
}
